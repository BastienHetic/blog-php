<?php ob_start() ?>
<div class="container">
    <h1>Formulaire d'inscription</h1>
    <form action="http://hetic.localhost/blog-php/?action=signin" method="POST">
        <div class="mb-3">
            <label for="inputLastname" class="form-label">Nom</label>
            <input type="text" name="lastname" class="form-control" id="inputLastname" aria-describedby="emailHelp">
        </div>
        <div class="mb-3">
            <label for="inputFirstname" class="form-label">Prénom</label>
            <input type="text" name="firstname" class="form-control" id="inputFirstname" aria-describedby="emailHelp">
        </div>
        <div class="mb-3">
            <label for="inputEmail" class="form-label">Email</label>
            <input type="email" name="email" class="form-control" id="inputEmail" aria-describedby="emailHelp">
            <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
        </div>
        <div class="mb-3">
            <label for="inputPassword" class="form-label">Mot de passe</label>
            <input type="password" name="password" class="form-control" id="inputPassword">
        </div>
        <input type="submit" name="submit" class="btn btn-primary" value="Valider">
        <input type="hidden" name="action" value="signin"/>
    </form>
</div>
