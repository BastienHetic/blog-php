<?php

require_once "model/userManager.php";
require_once "model/userModel.php";

class userController
{
    public function showAccount()
    {
        include 'view/account.php';
        return ob_get_clean();
    }

    public function login()
    {
        if(isset($_POST['sub']))
        {
            $email = $_POST['email'];
            $password = $_POST['password'];
            // $password = password_hash($_POST['password'], PASSWORD_DEFAULT);

            $userManager = new userManager;
            $res = $userManager->findByEmailPwd($email, $password);
            if($res->rowCount() > 0){
                $user = new User;
                $user->hydrate($res->fetchAll(PDO::FETCH_ASSOC));

                //require_once 'view/index.php';
            } else {
                //require_once 'view/register.php';
            }
        }

        return $res;
    }

    public function signin()
    {
        if (!empty($_POST['submit'])) {
            $user = new User();
            $user->hydrate($_POST);

            $userManager = new UserManager();
            if ($userManager->insert($user)) {
                $_SESSION['user'] = $user;
                header('Location: http://hetic.localhost/blog-php/?action=account');
            }
        }

        include 'view/signinForm.php';
        return ob_get_clean();
    }
}