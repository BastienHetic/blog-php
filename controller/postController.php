<?php

require_once 'model/postManager.php';

class postController
{
    public function showAll()
    {
        $postManager = new PostManager;
        $postManager->findAll();

        include 'view/listPosts.php';
        return ob_get_clean();
    }
}