<?php

require_once 'manager.php';

class userManager extends Manager
{
    /**
     * @param User $user
     * @return bool
     */
    public function insert(User $user): bool
    {
         $sth = $this->connection->prepare(
            'INSERT INTO user (firstname, lastname, email, password, role) 
            VALUES (:firstname, :lastname, :email, :password, :role)'
        );

        return $sth->execute([
            ':firstname' => $user->getFirstname(),
            ':lastname' => $user->getLastname(),
            ':email' => $user->getEmail(),
            ':password' => $user->getPassword(),
            ':role' => $user->getRole(),
        ]);
    }

    public function findByEmailPwd($email, $password) {
        $res = $this->connection->query("SELECT * FROM user WHERE email='$email' AND password='$password'");
        return $res;
    }
}