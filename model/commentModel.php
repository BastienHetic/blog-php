<?php 

class Comment
{
    private $id;
    private $comment;
    private $user;

    //GETTER
    public function getId(){
        return $this->id;
    }

    public function getComment(){
        return $this->comment;
    }

    public function getUser(){
        return $this->user;
    }

  
    // SETTER
    public function setId($id){
        $this->id = $id;
    }

    public function setComment($comment){
        $this->comment = $comment;
    }

    public function setUser($user){
        $this->user = $user;
    }
}