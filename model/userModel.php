<?php 
class User
{
    private $id;
    private $firstname;
    private $lastname;
    private $email;
    private $password;
    private $role;

    public function getId(){
        return $this->id;
    }

    public function getFirstname(){
        return $this->firstname;
    }

    public function getLastname(){
        return $this->lastname;
    }

    public function getEmail(){
        return $this->email;
    }

    public function getPassword(){
        return $this->password;
    }

    public function getRole(){
        return $this->role;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function setFirstname($firstname){
        $this->firstname = $firstname;
    }

    public function setLastname($lastname){
        $this->lastname = $lastname;
    }

    public function setEmail($email){
        $this->email = $email;
    }

    public function setPassword($password, $encrypt = true){
        if ($encrypt) {
            $password = self::encryptPassword($password);
        }
        $this->password = $password;
    }

    public function setRole($role){
        $this->role = $role;
    }

    private function encryptPassword($plainPassword)
    {
        return password_hash($plainPassword, PASSWORD_DEFAULT);
    }

    public function hydrate($data) {
        foreach ($data as $key => $datum) {
            $setter = 'set' . ucfirst($key);
            if (is_callable([__CLASS__, $setter])) {
                call_user_func_array([$this, $setter], [$datum]);
            }
        }
    }
}