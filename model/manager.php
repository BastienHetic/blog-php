<?php

class Manager
{
    private $host = 'localhost';
    private $user = 'root';
    private $password = '';

    protected $connection;

    public function __construct()
    {
        $this->setMySqlConnection();
    }

    private function setMySqlConnection()
    {
        $this->connection = new PDO('mysql:host=' . $this->host . ';dbname=projectback', $this->user, $this->password);
    }
}