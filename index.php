<?php

//ini_set("display_errors", 1);
//error_reporting(E_ALL);

session_start();
ob_start();

$action = $_POST['action'] ?? $_GET['action'];

switch($action) {
    case 'account':
        require_once 'controller/userController.php';
        $user = new userController();
        $content = $user->showAccount();
        break;
    case 'signin':
        require_once 'controller/userController.php';
        $user = new userController();
        $content = $user->signin();
        break;
    case 'login':
        require_once 'controller/userController.php';
        $user = new userController();
        $user->login();
        break;
    default:
        require_once 'controller/postController.php';
        $post = new PostController();
        $content = $post->showAll();
        break;
}

require_once 'view/base.php';
ob_end_flush();